export const enum PropertyType {
  APARTMENT = 1,
  HOUSE = 2,
}

export type ApartmentJson = {
  id: string;
  area: number;
  dateAdd: `${number}-${number}-${number}`;
  address: string;
  rooms: number;
  floor: number;
  images: string[];
  price: string;
  propertyType: PropertyType;
  location: [number, number];
};

export type ApartmentType = {
  id: string;
  area: number;
  dateAdd: Date;
  address: string;
  rooms: number;
  floor: number;
  images: string[];
  price: string;
  propertyType: PropertyType;
  location: [number, number];
};
