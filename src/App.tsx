import { useState } from 'react';
import { Map } from 'components/Map/Map';
import { ApartmentsList } from 'components/ApartmentsList/ApartmentsList';
import { AppHeader } from 'components/AppHeader/AppHeader';
import { AppMobileMenu } from 'components/AppMobileMenu/AppMobileMenu';
import { useApartments } from 'hooks/apartments';

import './App.css';

function App() {
  const [apartmentLocation, setApartmentLocation] =
    useState<[number, number]>();

  return (
    <div className="App">
      <AppHeader />

      <div className="App-body">
        <ApartmentsList
          apartments={useApartments()}
          onApartmentClick={apartment =>
            setApartmentLocation(() => apartment.location)
          }
        />
        <Map center={apartmentLocation} />
      </div>

      <AppMobileMenu />
    </div>
  );
}

export default App;
