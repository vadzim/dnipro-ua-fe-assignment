import { useMemo } from 'react';
import useFetch from 'use-http';
import type { ApartmentJson, ApartmentType } from 'structures/apartment';
import { parse } from 'date-fns';

export const useApartments = (): ApartmentType[] => {
  const { data = [] } = useFetch<ApartmentJson[]>('/data.json', {}, []);
  return useMemo(() => data.map(parseApartment), [data]);
};

const parseApartment = ({ dateAdd, ...rest }: ApartmentJson): ApartmentType => {
  const parsedDateAdd = parse(dateAdd, 'yyyy-mm-dd', new Date());

  return {
    ...rest,
    dateAdd: parsedDateAdd,
  };
};
