import './AppMobileMenu.css';

export const AppMobileMenu = () => (
  <div className="App-mobile-menu">
    <img className="show-map-button" src="location+maker+map+icon.svg" alt="" />
  </div>
);
