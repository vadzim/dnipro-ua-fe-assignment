import { Menu } from 'components/Menu/Menu';
import logo from 'logo.svg';

import './AppHeader.css';

export const AppHeader = () => (
  <header className="App-header">
    <img src={logo} className="App-logo" alt="logo" />
    <div className="App-menu">
      <div className="App-desktop-menu">
        <Menu />
      </div>
    </div>
  </header>
);
