import type { ReactNode } from 'react';
import './MenuItem.css';

export const MenuItem = ({ children }: { children: ReactNode }) => {
  return <div className="menu-item">{children}</div>;
};
