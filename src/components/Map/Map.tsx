import { useEffect, useRef, useState } from 'react';
import mapboxgl from 'mapbox-gl';

import 'mapbox-gl/dist/mapbox-gl.css';
import './Map.css';

mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';

type MapProps = {
  center?: [number, number];
};

export const Map = ({ center }: MapProps) => {
  const mapContainerRef = useRef(null);
  const [map, setMap] = useState<mapboxgl.Map | null>();

  // Initialize map when component mounts
  useEffect(() => {
    const mapboxglMap = new mapboxgl.Map({
      container: mapContainerRef.current as unknown as HTMLElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [30.622714, 50.43337],
      zoom: 12,
    });

    setMap(mapboxglMap);

    return () => mapboxglMap.remove();
  }, []);

  // update map center
  const [lat, lon] = center || [undefined, undefined];

  useEffect(() => {
    if (lat != null && lon != null) {
      map?.setCenter([lon, lat]);
    }
  }, [map, lat, lon]);

  return (
    <div className="App-map">
      <div className="map-container" ref={mapContainerRef} />
      {map && <MapZoomPanel map={map} />}
    </div>
  );
};

//
//
const MapZoomPanel = ({ map }: { map: mapboxgl.Map }) => (
  <div className="map-zoom-panel">
    <div
      role="button"
      className="map-zoom-button map-zoom-in"
      onClick={() => map.zoomIn()}
    >
      <span>+</span>
    </div>
    <div
      role="button"
      className="map-zoom-button map-zoom-out"
      onClick={() => map.zoomOut()}
    >
      <span>&ndash;</span>
    </div>
  </div>
);
