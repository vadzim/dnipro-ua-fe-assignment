import { PropertyType, ApartmentType } from 'structures/apartment';
import { format } from 'date-fns';

import './Apartment.css';

type ApartmentProps = {
  apartment: ApartmentType;
  onClick?: () => void;
};

export const Apartment = ({ apartment, onClick }: ApartmentProps) => {
  const { area, dateAdd, address, rooms, floor, images, price, propertyType } =
    apartment;

  return (
    <div className="apartment" onClick={onClick}>
      <div className="apartment-image-container">
        <div
          className="apartment-image"
          style={{
            backgroundImage: images[0] ? `url("${images[0]}")` : undefined,
          }}
        />
      </div>
      <div className="apartment-info">
        <div className="apartment-price">
          ${Intl.NumberFormat('ru').format(Number(price))}
        </div>

        <div className="apartment-type">{getPropertyName(propertyType)}</div>

        <div className="apartment-address">{address}</div>

        <div className="apartment-description">
          <div>{rooms} room</div>
          <div>{floor} floor</div>
          <div>{area}m²</div>
        </div>

        <div className="apartment-date-add">
          {format(dateAdd, 'd MMM yyyy')}
        </div>
      </div>
    </div>
  );
};

const getPropertyName = (propertyType: PropertyType) => {
  switch (propertyType) {
    case PropertyType.APARTMENT:
      return 'Apartment';
    case PropertyType.HOUSE:
      return 'House';
    default:
      throw new Error('Unknown property type');
  }
};
