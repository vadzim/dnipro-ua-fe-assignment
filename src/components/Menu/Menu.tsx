import { useState } from 'react';
import classnames from 'classnames';
import { MenuItem } from '../MenuItem/MenuItem';
import './Menu.css';

export const Menu = () => {
  const [menuVisible, setMenuVisible] = useState(false);

  return (
    <div className="menu-container">
      <div
        className="menu-mobile"
        onClick={() => setMenuVisible(visible => !visible)}
      >
        <img className="menu-button" src="menu.svg" alt="" />
      </div>
      <div
        className={classnames([
          'menu',
          menuVisible ? 'menu-visible' : 'menu-hidden',
        ])}
      >
        <MenuItem>Sale</MenuItem>
        <MenuItem>Rent</MenuItem>
        <MenuItem>Short term</MenuItem>
      </div>
    </div>
  );
};
