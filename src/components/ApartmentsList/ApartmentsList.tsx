import { Apartment } from 'components/Apartment/Apartment';
import { ApartmentType } from 'structures/apartment';

import './ApartmentsList.css';

type ApartmentsListProps = {
  apartments: ApartmentType[];
  onApartmentClick: (apartment: ApartmentType) => void;
};

export const ApartmentsList = ({
  apartments,
  onApartmentClick,
}: ApartmentsListProps) => {
  return (
    <div className="appartment-list">
      {apartments.map(apartment => (
        <Apartment
          key={apartment.id}
          apartment={apartment}
          onClick={() => onApartmentClick(apartment)}
        />
      ))}
    </div>
  );
};
